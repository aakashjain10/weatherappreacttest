import React, { Component, Suspense } from 'react';
import { Route, BrowserRouter as Router  } from 'react-router-dom'  
import App from './App';

const CardsComponant = React.lazy(() => import('./componants/cards/CardsComponant'));  
const CardDetailsComponant = React.lazy(() => import('./componants/cardDetails/CardDetailsComponant'));  
const NewLoginComponant = React.lazy(() => import('./componants/login/NewLoginComponant'));  
const WelcomeComponant = React.lazy(() => import('./componants/welcome/WelcomeComponant'));  
const ActivityIndicatorComponent = React.lazy(() => import('./shared/activityIndicator/ActivityIndicatorComponent'));  
const AppHeaderComponant = React.lazy(() => import('./shared/header/AppHeaderComponant')); 
const SideMenuComponant = React.lazy(() => import('./shared/sideMenu/SideMenuComponant')); 
const AppBarComponant = React.lazy(() => import('./shared/appBar/AppBarComponant')); 
const ProfileController = React.lazy(() => import('./componants/profile/ProfileController')); 

class AppRouter extends Component {
    render() {
       return (
        <Suspense fallback={<div>Loading...</div>}>  
            {/* <AppBarComponant /> */}
            {/* <SideMenuComponant /> */}
            <ActivityIndicatorComponent />
            <AppHeaderComponant showHeader={false} />
            <Router>
                <Route exact path = "/" component = {App} />
                <Route path = "/welcome" component = {WelcomeComponant} />
                <Route path = "/newlogin" component = {NewLoginComponant} />
                <Route path = "/cards" component = {CardsComponant} />
                <Route path = "/card/details" component = {CardDetailsComponant} />
                {/* <Route path = "/profile" component = {ProfileController} />  Not Yet Completed */} 
            </Router> 
                
        </Suspense>  
       )
    }

  }
  export default AppRouter;

