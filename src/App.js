import React, {Component} from 'react';
import './App.css';
import SelectedDevice from './shared/pojo/SelectedDevice';   
import { withLDProvider } from 'launchdarkly-react-client-sdk';
import EnvironmentConfig from "./shared/webUtility/EnvironmentConfig";

class App extends Component {

  constructor() {
     super();
     this.redirectTo = this.redirectTo.bind(this);
     this.selectedDevice = new SelectedDevice().getData();
  }

  componentWillMount(){
      EnvironmentConfig.setEnvironment("ITG");
  }

  componentDidMount() {
      var isAlreadyLoggedIn = true;

      if(isAlreadyLoggedIn) {
         this.redirectTo("/cards");
      } else {
         this.redirectTo("/welcome");
      }
   }

  redirectTo = (path) => {
      this.props.history.push(path);
   }

   render() {
      return (<></>);
   }
 }
 export default App;

//  export default withLDProvider({ 
//    clientSideID:  EnvironmentConfig.getEnvironment().LdClientSideID,
//    user: {
//        "key": "user_key",
//        "name": "User Name",
//        "email": "User@email.com"
//    }
//  })(App);
