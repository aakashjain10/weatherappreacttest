import React, {Component} from 'react';
import { withLDConsumer } from 'launchdarkly-react-client-sdk';
//import { Button } from '@veneer/core';
// const HelloWorld = ({ flags }) => {
//     return flags.showProfile ? <div>Flag on</div> : <div>Flag off</div>;
// };

// export default withLDConsumer()(HelloWorld);

class HelloWorld extends Component {
    constructor() {
       super();
    }
  
     render() {

      //return (<Button id="primary-id"> Primary </Button>);
      return (this.props.flags.showProfile ? <div>Flag on</div> : <div>Flag off</div>);
     }
   }
   export default withLDConsumer()(HelloWorld);