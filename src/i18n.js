import i18n from "i18next";
import { initReactI18next } from "react-i18next";

import translationEN from './translations/en-US.json';
import translationDE from './translations/de-DE.json';


// the translations
const resources = {
  en: {
    translation: translationEN
  },
  de: {
    translation: translationDE
  }
};

i18n
  .use(initReactI18next) // passes i18n down to react-i18next
  .init({
    resources,
    lng: "en",

    keySeparator: false, // we do not use keys in form messages.welcome

    interpolation: {
      escapeValue: false, // react already safes from xss
      format: function(value, format, lng) {
        if (format === 'uppercase') return value.toUpperCase();
        //if(value instanceof Date) return moment(value).format(format);
        return value;
      }
    }
  });

  global.i18n = i18n;
export default i18n;