import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import EnvironmentConfig from '../../shared/webUtility/EnvironmentConfig';

class WelcomeComponent extends Component {

    constructor() {
        super();
        this.redirectTo = this.redirectTo.bind(this);
        this.openHPID = this.openHPID.bind(this);
    }

    UNSAFE_componentWillMount() {

    }

    UNSAFE_componentDidMount() {

    }

    redirectTo = (path) => {
        this.props.history.push(path);
    }

    openHPID = () => {
        window.location.assign(EnvironmentConfig.getEnvironment().HPID_URL);
    }

    render() {
        return (
            <div>Welcome To Eagle<br></br>             
                <Button className="primary" onClick={this.openHPID } >Login</Button>
            </div>
            );
        }
    }
    export default WelcomeComponent;