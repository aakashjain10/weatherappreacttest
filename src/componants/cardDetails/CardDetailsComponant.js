import React from 'react';

import Row from 'react-bootstrap/Row'
import Container from 'react-bootstrap/Container'
import Col from 'react-bootstrap/Col'
import Button from 'react-bootstrap/Button'

import SelectedDevice from '../../shared/pojo/SelectedDevice';   
import i18n from "../../i18n";

class CardDetailsComponant extends React.Component {

   constructor() {
      super();
      this.redirectTo = this.redirectTo.bind(this);
      this.selectedDevice = new SelectedDevice().getData();
   }

   redirectTo = (path) => {
      this.props.history.push(path);
    }

    render() {

      if(this.selectedDevice == null) {
         this.redirectTo("/");
         return (null);
      }

       return (
          <div>
             <div>{i18n.t('Card Details Page...')}</div>
            
             <span>Selected Day</span> <br></br>  <br></br> 
             <div>Id: {this.selectedDevice.id}</div>
             <div>Name: {this.selectedDevice.dayName}</div>
             <div>Temperature: {this.selectedDevice.temperature}</div>

             <Container>
               <Row>
                  <Col xs={12} sm={12} md={4}>I am One</Col>
                  <Col xs={12} sm={12} md={4}>I am Two</Col>
                  <Col xs={12} sm={12} md={4}>I am Three</Col>
               </Row>
               <Row>
                  <Col>1 of 3</Col>
                  <Col>2 of 3</Col>
                  <Col>3 of 3</Col>
               </Row>
            </Container>
          </div>
       )
    }
  }
  export default CardDetailsComponant;