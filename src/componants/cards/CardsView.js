import React from 'react';
import styles from '../../assets/css/cards.module.css';   
import Col from 'react-bootstrap/Col'

class CardsView extends React.Component {
    render() {
       return (
          <Col xs={12} sm={12} md={4} lg={4} onClick={() => this.props.onClick(this.props.data) }>
             <div className={styles.gridItem}>
             <span>{this.props.data.dayName}</span> <br></br>
             <span>{global.i18n.t('celsius', {degree: this.props.data.temperature})}</span>
             </div>
          </Col>
         
       )
    }
  }
export default CardsView;