import React from 'react';
import CardsView from './CardsView'; 
import SelectedDevice from '../../shared/pojo/SelectedDevice';   
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Modal from 'react-bootstrap/Modal';
import WebServiceUtility from '../../shared/webUtility/WebServiceUtility';
import {DEVICES} from '../../shared/webUtility/WebserviceEndPoints';
import { Button } from '@veneer/core';
import style from '../../assets/css/cards.componants.module.css';

class CardsComponant extends React.Component {
    constructor() {
      super();
      this.state = {

         // Dummy data can be moved to seperate file
         data: 
         [
            {
               "id":1,
               "dayName": "Monday",
               "temperature":"20"
            },
            {
               "id":2,
               "dayName": "Tuesday",
               "temperature":"30"
            },
            {
               "id":3,
               "dayName": "Wednesday",
               "temperature":"40"
            },
            {
               "id":4,
               "dayName": "Thursday",
               "temperature":"50"
            }
         ],
         show: false,
         showLoader: false
      }

      this.onItemClick = this.onItemClick.bind(this);
      this.redirectTo = this.redirectTo.bind(this);
      this.handleShow = this.handleShow.bind(this);
      this.handleClose = this.handleClose.bind(this);

   }

   componentDidMount() {
      WebServiceUtility.request(DEVICES.endpointURL, DEVICES.methodType, {},(success) => {
         console.log(success);
     }, (error) => {
         console.log(error); 
      });
   }

   onItemClick = (obj) => {
      new SelectedDevice().setData(obj);
      this.redirectTo("/card/details");
   }

   handleShow = () => {
      this.setState({show: true});  
   }

   handleClose = () => {
      this.setState({show: false});  
   }

   redirectTo = (path) => {
      this.props.history.push(path);
   }

    render() {
       return (
          <>
            <Button className="primary" onClick={this.handleShow } >Show Modal</Button>

            <Container className={style.container}>
               <Modal show={this.state.show} onHide={this.handleClose} centered>
                  <Modal.Header closeButton>
                     <Modal.Title>{global.i18n.t('Modal heading')}</Modal.Title>
                  </Modal.Header>
                  <Modal.Body>Woohoo, you're reading this text in a modal!</Modal.Body>
                  <Modal.Footer>
                     <Button variant="secondary" onClick={this.handleClose}> Close </Button>
                     <Button variant="primary" onClick={this.handleClose}> Save Changes </Button>
                  </Modal.Footer>
               </Modal>

               <Row style={{marginLeft: 0, marginRight: 0 }}> 
                  {this.state.data.map((person, i) => <CardsView key = {i} data = {person} onClick = {this.onItemClick} />)}
               </Row> 
            </Container>
         </>
       )
    }
  }

  export default CardsComponant;