import React from 'react'
import ProfileController from './ProfileController'
import ProfileViewModel from './ProfileViewModal';

class ProfileComponent extends React.Component {

    constructor(props) {
        super(props)
        const pokemonModel = props[RootStore.type.POKEMON_MODEL]
        this.viewModel = new ProfileViewModel(pokemonModel)
    }

    render() {
        return (
            <ProfileController  viewModel={this.viewModel} />
        )
    }
}

export default ProfileComponent