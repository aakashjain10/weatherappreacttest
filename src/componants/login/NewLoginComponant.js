import React, {Component} from 'react';

class NewLoginComponant extends Component {

    constructor() {
       super();
       this.redirectTo = this.redirectTo.bind(this);
    }
  
    componentWillMount(){
        
    }
  
    componentDidMount() {
       
      console.log(window.location.search);

      var urlParams = new URLSearchParams(window.location.search);
      console.log(urlParams.get('code'));

      this.redirectTo("/cards");
   }
  
    redirectTo = (path) => {
        this.props.history.push(path);
     }
  
     render() {
        return (<></>);
     }
   }

   export default NewLoginComponant;