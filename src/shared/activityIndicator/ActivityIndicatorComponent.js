import React, { Component } from 'react';
import styles from '../../assets/css/activityIndicator.module.css';   

class ActivityIndicatorComponent extends Component {

    render() {
        return (
            <div id="activity-indicator" className={styles.divStyle}>
                <img className={[styles.imgStyle]} alt='' src={ require('../../assets/img/giphy.gif') } />  
            </div>
        )
    }
}

 export default ActivityIndicatorComponent