import React, { Component } from 'react';
import { SideMenu, SideMenuItem} from '@veneer/core';

class SideMenuComponant extends Component {

     render() {
        return (            
          <SideMenu theme="hp" logoImagePath="http://veneer.lightaria.com:3000/assets/foundation/veneer-logo.svg">
            <SideMenuItem label="John Doe" icon="user">
              <SideMenuItem label="Logout" />
            </SideMenuItem>
            <SideMenuItem label="Home" icon="home" />
            <SideMenuItem label="Products" />
            <SideMenuItem label="Categories" />
          </SideMenu>
        );
     }
   }

   export default SideMenuComponant;