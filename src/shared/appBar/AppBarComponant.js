import React, { Component } from 'react';
import { AppBar, AppBarLeftSection, Icon } from '@veneer/core';

class AppBarComponant extends Component {

     render() {
        return (            
            <AppBar  theme="veneer" menuEnabled={true} style={{ position: "relative", zIndex: 10 }} className="undefined">
                <AppBarLeftSection>
                    <Icon name="hp" color="white" className="items-space" /> <h4>Title</h4>
                </AppBarLeftSection>
            </AppBar>
        );
    }
}

export default AppBarComponant;