export default class EnvironmentConfig {

    static setEnvironment = (environment) =>  {
        localStorage.setItem("HostEnvironment", environment);
    }

    static getEnvironment = () =>  {
        var env = localStorage.getItem("HostEnvironment");

        switch(env) {
    
            case "ITG":
                return   {
                    "LdClientSideID": "5d5e6621963363087d6a615e", 
                    "BaseUrl" : "https://api2-itg-methone.hpcloud.hp.com/itg-v3/",
                    "HPID_URL": "https://directory.stg.cd.id.hp.com/directory/v1/oauth/logout?state=en&post_logout_redirect_uri=https://content-itg.methone.hpcloud.hp.net/profile/logout9.html",
                    "HPID_REDIRECT_URL": "https://content-itg.methone.hpcloud.hp.net/profile/index9.html"
                }
    
            case "STG":
                return   {
                    "LdClientSideID": "5d5e6621963363087d6a615e", 
                    "BaseUrl" : "https://api2-itg-methone.hpcloud.hp.com/itg-v3/",
                    "HPID_URL": "https://directory.stg.cd.id.hp.com/directory/v1/oauth/logout?state=en&post_logout_redirect_uri=https://content-itg.methone.hpcloud.hp.net/profile/logout9.html",
                    "HPID_REDIRECT_URL": "https://content-itg.methone.hpcloud.hp.net/profile/index9.html"
                }
    
            case "PRO":
                return   {
                    "LdClientSideID": "5d5e6621963363087d6a615f", 
                    "BaseUrl" : "https://api2-methone.hpcloud.hp.com/v3/",
                    "HPID_URL": "",
                    "HPID_REDIRECT_URL": ""
                }
            default:
                return {
                    "LdClientSideID": "5d5e6621963363087d6a615e", 
                    "BaseUrl" : "https://api2-itg-methone.hpcloud.hp.com/itg-v3/",
                    "HPID_URL": "https://directory.stg.cd.id.hp.com/directory/v1/oauth/logout?state=en&post_logout_redirect_uri=https://content-itg.methone.hpcloud.hp.net/profile/logout9.html",
                    "HPID_REDIRECT_URL": "https://content-itg.methone.hpcloud.hp.net/profile/index9.html"
                }
        }
    
    }
}



// https://jsonplaceholder.typicode.com/posts/1
