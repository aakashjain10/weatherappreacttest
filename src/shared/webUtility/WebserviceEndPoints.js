export const DEVICES = {
    endpointURL: "devices/",
    methodType: "GET"
}

export const DEVICES_DETAILS = {
    endpointURL: "devices/details/",
    methodType: "GET"
}

export const PROFILE = {
    endpointURL: "profile/",
    methodType: "GET"
}