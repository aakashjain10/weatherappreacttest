import EnvironmentConfig from './EnvironmentConfig';
import ReactDOM from 'react-dom';

class WebServiceUtility {

    static request = (endpointURL, methodType, queryParameter, successCallback, errorCallback) =>  {
        var selectedEnvironment = EnvironmentConfig.getEnvironment();

        var url = selectedEnvironment.BaseUrl + endpointURL;

        showLoader();

        return (
            fetch(url, {
                method: methodType,
                headers: GetHeader.getInstance().getHeader(false),
                body: getBody(queryParameter)
            })
            .then((response) => response.json())
            .then((responseJson) => {
                successCallback(responseJson);    
                hideLoader();
            })
            .catch((error) => {
                errorCallback(error);
                hideLoader();
            })

        );
    }
}

function getBody(queryParameter) {
    for(var key in queryParameter) {
        if(queryParameter.hasOwnProperty(key))
            return queryParameter;
    }
    return null;
}

class GetHeader {
    static instance = null;
    static getInstance() {
        if (GetHeader.instance == null) {
            GetHeader.instance = new GetHeader();
        }

        return this.instance;
    }

    getHeader = (skipAuthorisation) => {
        if(skipAuthorisation) {
            return {
                'x-api-key': 'nL8rZESmAA4sSX0Q9R37l5vr5zJwRwiZ5pm71zK6',
                'Content-Type': 'application/json',
            }
        }

        return {
            'x-api-key': 'nL8rZESmAA4sSX0Q9R37l5vr5zJwRwiZ5pm71zK6',
            'Content-Type': 'application/json',
            'Authorization': 'HPSA CustomerId="4", SessionToken="b1472e95297ac086182f06de3c110542", CallerId="iostest", AppVersion="1.4.0", Platform="ios"'        
        }
    }
}

function showLoader(){
    ReactDOM.findDOMNode(document.getElementById('activity-indicator')).style.display = 'block';
}

function hideLoader(){
    ReactDOM.findDOMNode(document.getElementById('activity-indicator')).style.display = 'none';
}

export default WebServiceUtility;


